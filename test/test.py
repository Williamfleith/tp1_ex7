# -*- coding: utf-8 -*-
"""
Created on 03/04/2020
by William FLEITH
"""
import sys
import unittest
import logging

# Insert in the first value of the PythonPath list the PYTHONPATH corresponding to this exercise.
sys.path.insert(0, "/home/tp/AGC_TP/tp1_ex7")
from calculator.ex7 import SimpleCalculator


class Test(unittest.TestCase):
    """
    Class unittest, with 2 methods to do test concerning sum in the first and divide in the second.
    We could have done many other tests in this class.
    For exemple concerning the limit of number acceptable.
    """

    def test_sum(self):
        """
        function test_sum, use the code to do the sum then check if the result is the one we wanted.
        assertEqual is use to check for an expected result.
        """
        LOG.info("start of test_sum, basic case with positive and negative integer")

        test = SimpleCalculator(2, 1)
        result = test.sum()
        self.assertEqual(result, 3)

        test = SimpleCalculator(2, -1)
        result = test.sum()
        self.assertEqual(result, 1)

        test = SimpleCalculator(-1, 2)
        result = test.sum()
        self.assertEqual(result, 1)

        test = SimpleCalculator(-2, -1)
        result = test.sum()
        self.assertEqual(result, -3)

        LOG.info("end of test_sum, basic case with positive and negative integer\n")

    def test_divide(self):
        """
        function test_divide to keep from division by 0
        """
        LOG.info("start of test_divide, basic case with positive and negative integer")

        test = SimpleCalculator(2, 1)
        result = test.divide()
        self.assertEqual(result, 2)

        test = SimpleCalculator(2, 0)
        #launching the test with the raise of ZeroDivisionError
        with self.assertRaises(ZeroDivisionError):
            test.divide()

        LOG.info("end of test_divide, basic case with positive and negative integer\n")


# Main code, call the unittest class.
if __name__ == "__main__":
    """
    the point of logging are to create build a system that show the errors properly in categories :
    info, debug, ...
    """

    LOG = logging.getLogger("test")
    #we print all the things with a level superior of debug
    LOG.setLevel(logging.INFO)
    #all the information will be in a file named file.log
    HANDLER = logging.FileHandler(filename="file.log", mode="w")

    HANDLER.setLevel(logging.DEBUG)

    FORMATTER = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    HANDLER.setFormatter(FORMATTER)
    LOG.addHandler(HANDLER)

    unittest.main()
