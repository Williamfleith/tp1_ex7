**Readme Ex7:**

Created on 22/04/2020 by William FLEITH


**Objective :** We added to the ex6 files the module logging the enable a tracability on errors and understand them easier

**Pylint :** The programm ex7.py is rated 10/10. But the test.py is only rated 9.5/10 because of the line for the python path taht can't be placed first but it doesn't affect the functionaly of the programm (it is the same in ex4, 5 and 6 and it will be the same until we created a setup.py file for the pythonpath).


**Running :**
 To run the programm go to the package test and run test.py
- The line : sys.path.insert(0, "/home/tp/AGC_TP/tp1_ex7") enable not to setup the PYTHONPATH every time, it will do it automatically everytime we will run the programm.
- Becareful to change this line (line 9) on test/test.py with your absolute right way.
- Files with extension .pyc are removed from the repository.
- Init files are only for the initiation of packages so they are likely to be empty.

**Logging :**
We used the module logging to build a file file.log that show error in differents categories.
- after changing pythonpath as mentioned above launch test.py and open file.log to see the result.